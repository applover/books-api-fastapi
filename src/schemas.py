from datetime import datetime
from typing import Optional

from pydantic import BaseModel


class BookBase(BaseModel):
    serial_number: str
    title: str
    author: str


class BookCreate(BookBase):
    pass


class BookUpdate(BaseModel):
    is_borrowed: bool
    borrowed_by: Optional[str] = None
    borrowed_at: Optional[datetime] = None


class Book(BookBase):
    id: int
    is_borrowed: bool
    borrowed_by: Optional[str] = None
    borrowed_at: Optional[datetime] = None

    class Config:
        orm_mode = True
