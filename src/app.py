from contextlib import asynccontextmanager

from fastapi import Depends, FastAPI, Request
from sqlalchemy.orm import Session

import crud
import models
import schemas
from db import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)


@asynccontextmanager
async def lifespan(app: FastAPI):
    app.state.session = SessionLocal()
    yield
    app.state.session.close()


app = FastAPI(lifespan=lifespan)


@app.post("/books/", response_model=schemas.Book)
def create_book(book: schemas.BookCreate, request: Request):
    db = request.app.state.session
    return crud.create_book(db=db, book=book)


@app.delete("/books/{book_id}")
def delete_book(book_id: int, request: Request):
    db = request.app.state.session
    return crud.delete_book(db=db, book_id=book_id)


@app.get("/books/", response_model=list[schemas.Book])
def read_books(request: Request, skip: int = 0, limit: int = 10):
    db = request.app.state.session
    books = crud.get_books(db, skip=skip, limit=limit)
    return books


@app.put("/books/{book_id}", response_model=schemas.Book)
def update_book(book_id: int, book: schemas.BookUpdate, request: Request):
    db = request.app.state.session
    return crud.update_book(db=db, book_id=book_id, book=book)
