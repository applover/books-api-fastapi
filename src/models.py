from sqlalchemy import Boolean, Column, DateTime, Integer, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Book(Base):
    __tablename__ = "books"

    id = Column(Integer, primary_key=True, index=True)
    serial_number = Column(String, unique=True, index=True)
    title = Column(String, index=True)
    author = Column(String, index=True)
    is_borrowed = Column(Boolean, default=False)
    borrowed_by = Column(String, nullable=True)
    borrowed_at = Column(DateTime, nullable=True)
