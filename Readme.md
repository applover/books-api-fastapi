# Bookstore API

Fast API Book API for AppLover team to validate
- [Overall](#overall)
  - [Document scope and versioning](#document-scope-and-versioning)
  - [Run project](#run-project)

# Overall
## Document scope and versioning

This document is an introduction for project developers. It briefly describes a project, introduces developer into a project.

| date       | version | change description | author                            |
|------------|---------|--------------------|-----------------------------------|
| 20.06.2024 | 1.0.0   | initial version    | Piotr Liwerski |


## Run project
Use a command ```make run``` \
NOTES:
1. App is hosted on 8000 port
2. During development I got
```bash
 ✘ book-postgres Error Head "https://registry-1.docker.io/v2/library/postgres/manifests/...                      1.0s
Error response from daemon: Head "https://registry-1.docker.io/v2/library/postgres/manifests/16-alpine": received unexpected HTTP status: 503 Service Unavailable
```
So I need to use sqlite for now. It will create test.db file in src directory.
3. Swagger is on ```/docs```
